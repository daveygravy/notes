# Environmental Impacts
- gas emissions
	- greenhouse gas 
		- thermal radiation from earth -> infrared
			- easily trapped by GHGs
- interglacial periods
	- temperature increase due to high insolation
		- CO<sub>2</sub> released from ocean
	- solar forcing
	- sulfur dioxide
	- fluorides
	- polyaromatic hydrocarbons (PAH)
- dust emissions
- water emissions

#EIIP #climatechange 