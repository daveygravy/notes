# Operations Research
## [[Linear Programming]]
- [Simplex Method](Simplex_Method.md)
	- [Two-Phase Simplex Method](Two-Phase_Simplex_Method.md)
	- [Big M Method](Big_M_Method.md)


## Nonlinear Programming
- [KKT Conditions](KKT_Conditions.md)
- [Quadratic Programming](Quadratic_Programming.md)
- [Separable Programming](Separable_Programming.md)
- [Convex Porgramming](Convex_Programming.md)
	- [Gradient Search](Gradient_Search.md)
	- [Sequential Unconstrained Minimization Technique (SUMT)](Sequential_Unconstrained_Minimization_Technique.md)
	- [Frank-Wolfe Algorithm](Frank-Wolfe_Algorithm.md)

## Case Studies
- [School Timetable Optimization](School_Timetable_Optimization.md)
- [Travelling Salesman Problem](Travelling_Salesman_Problem.md)
- [Vehicle Routing](Vehicle_Routing.md)
- [Crew Scheduling in the Airline Industry](Crew_Scheduling_in_the_Airline_Industry.md)
- [Optimized implementation of Self-Service registers](Optimized_implementation_of_Self-Service_registers.md)
- [Robust Optimization and Aircraft Parts](Robust_Optimization_and_Aircraft_Parts.md)