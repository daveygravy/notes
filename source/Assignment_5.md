# T863 EIIP, Energy in industrial processes, 20 21 

## Assignment 5 

## Teacher: Guðrún Sævarsdóttir 

Be prepared to discuss the following scenario in class, Oct 18<sup>th</sup>. 

**Group discussion** 

As a group, you should be prepared to deliver proposals for the following problem: 

You are in charge of the potroom at an aluminium smelter. 

You were instructed by the company CEO/President to increase the pot line amperage in April 20 21 by 10 kA in order to increase the company profits. This was done without making any other changes in operational parameters/work practices. 

**However:** For the past several months
- [[Anode effects]] increase from 0.2 to 0.45 AE/pd 
- AlF<sub>3</sub> consumption increased 
- Noise has increased 
- Average bath temperature increased by 5.7°C 
- Anode carbon consumption has gone up 
- Anode burn-offs increased 
- Average cell voltage increased from 4.3 to 4.4 V 
- No. of cells with red sidewalls increased 
- No. of cathode failures increased 

Please be prepared to explain **why**? And **what should have been done**? 

**Given:** 
- Line amperage 175 kA 
- Current Efficiency 93% 
- Metal Depth 16 cm 
- kgAl/pd 1312 
- Bath Depth 18 cm 
- Cathode sidewall lining anthracite block 
- No. Cells 200 
- Alumina feed point feeder 
- ACD (at lowest limit) 4.0 cm 
- Anode current density 0.85 A/cm<sup>2</sup> 
- Anode 50 cm x 206 cm 
- No. Anodes 20 

### Discussion
- "ensure enough solute alumina in the electrolyte or else current will start to electrolyze the fluorine compounds in the electrolyte"
#### In-class
- need to increase alumina feeding proportional to current increase
- higher current -> more carbon consumption
	- natural

Side ledge melting away
- increase heat loss
	- decrease anode-cathode distance
		- may risk short-circuiting
			- take measures to stabilize metal
	- raise metal depth
	- lower thickness of anode cover

Voltage increase/noise
- bubbles	
- unstable movement of metal

Anode burn-off
- short-circuiting due to movement of metal
- protruding "wart" on bottom of anode
	- dusting in the cell

tags: #aluminumproduction #EIIP #casestudy