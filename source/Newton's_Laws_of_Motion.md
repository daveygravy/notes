# Newton's Laws of Motion
1. ***Inertia***
	- An object at rest will stay at rest and an object in motion will stay in motion, unless affected by an external force
$$\sum\vec{F}=0$$
2. ***Force***
	- Force is equal to change in momentum per unit time
	$$\sum\vec{F}=\frac{d(m\vec{v})}{dt}=m\frac{d\vec{v}}{dt}=ma$$
3. ***Action and Reaction***
	- A force by one object on another object has an equal and opposite reaction on the first object